import React from 'react';
import { StyleSheet, Image, View, TouchableOpacity, Text } from 'react-native';

const Quizz = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={{
            uri:
              'https://images.jedessine.com/_uploads/_tiny_galerie/20131041/pokemon-quizz_2ss.jpg',
          }}
          style={styles.image}
        />
      </View>
      <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center', marginVertical: 20 }}>
        Quel Pokémon est connu pour avoir une coquille de couleur rouge et évolue en un Pokémon de type Eau et Glace ?
      </Text>

      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => console.log('Réponse 1')}
        >
          <Text style={styles.buttonText}>Réponse 1</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => console.log('Réponse 2')}
        >
          <Text style={styles.buttonText}>Réponse 2</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => console.log('Réponse 3')}
        >
          <Text style={styles.buttonText}>Réponse 3</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Result')}
        >
          <Text style={styles.buttonText}>Réponse 4</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.answerContainer}>
        <Text style={{ marginTop: 30, color: 'green', fontSize: 20, fontWeight: 'bold' }}> Réponse!!</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    justifyContent: 'flex-start',
  },
  imageContainer: {
    flex: 0,
    width: '100%',
    alignSelf: 'stretch',
    alignItems: 'center',
    marginTop: 0,
  },
  image: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  buttonsContainer: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  answerContainer: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginVertical: 10,
    borderColor: 'blue',
    borderWidth: 2,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 50,
    borderRadius: 10,
    marginTop: 0,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Quizz;
