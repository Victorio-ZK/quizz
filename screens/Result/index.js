import React from 'react';
import {TouchableOpacity, StyleSheet, Image, View, Button, Text } from 'react-native';

const Result = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={{
            uri:
              'https://sg.portal-pokemon.com/play/resources/pokedex/img/quiz/title.png',
          }}
          style={styles.image}
        />
        <Text style={{ marginTop: 50, fontSize: 20, fontWeight: 'bold' }}>Score #/3</Text>
      </View>
      <View style={styles.buttonContainer}>

         <TouchableOpacity style={styles.buttonLeave} onPress={() => navigation.navigate('Home')}>
           <Text style={styles.buttonText}>Quitter le Quizz</Text>
         </TouchableOpacity>

         <TouchableOpacity style={styles.buttonRetry} onPress={() => navigation.navigate('Quizz')}>
           <Text style={styles.buttonText}>Recommencer</Text>
         </TouchableOpacity>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    justifyContent: 'center',
  },
  imageContainer: {
    flex: 2,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  image: {
    width: 300,
    height: 200,
    marginTop: 50,
  },
  buttonLeave: {
     marginVertical: 10,
     borderColor: 'red',
     borderWidth: 2,
     backgroundColor: 'red',
     justifyContent: 'center',
     alignItems: 'center',
     width: 300,
     height: 50,
     borderRadius: 10,
     marginTop: 0,
    },
    buttonRetry: {
       marginVertical: 10,
       borderColor: 'blue',
       borderWidth: 2,
       backgroundColor: 'blue',
       justifyContent: 'center',
       alignItems: 'center',
       width: 300,
       height: 50,
       borderRadius: 10,
       marginTop: 0,
    },
  buttonContainer: {
    flex: 2,
    marginTop: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold',
    },
});

export default Result;
