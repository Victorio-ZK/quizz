import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screens/Home';
import Quizz from '../screens/Quizz';
import Result from '../screens/Result';


 const Stack = createNativeStackNavigator();

 const MyStack = () => {
   return (
     <NavigationContainer>
       <Stack.Navigator>
         <Stack.Screen
           name="Home"
           component={Home}
           options={{title: 'Quizz-Pokemon'}}
         />
         <Stack.Screen name="Quizz"
          component={Quizz}
          options={{title: 'Page-Quizz'}}/>
       <Stack.Screen name="Result"
       component={Result}
       options={{title: 'Page-Result'}}/>
       </Stack.Navigator>
     </NavigationContainer>
   );
 };

 export default MyStack;