import React from 'react';
import {View, Text} from 'react-native';
import Routes from './routes/index.js';

const App =()=>{
    return(
    <View style={{flex: 1}}>
      <Routes />
    </View>
    );
}

export default App;